$('#myModal').on('hide.bs.modal', function (e) {
    if (location.hash == '#modal') window.history.back();
});

$(window).on('popstate', function (event) {
    if (event.state !== null) $('.modal').modal('hide');
});

$('.modal').on('shown.bs.modal', function () {
    document.getElementById('nn').value = localStorage.getItem('nn');
    document.getElementById('mail').value = localStorage.getItem('mail');
    document.getElementById('mesg').value = localStorage.getItem('mesg');
    if (localStorage.getItem('box') === "true") {
        $('#box').prop('checked', true);
    }
    else { $('#box').prop('checked', false); }
});

$('.modal').on('hidden.bs.modal', function () {
    localStorage.setItem('nn', $('#nn').val());
    localStorage.setItem('mail', $('#mail').val());
    localStorage.setItem('mesg', document.getElementById('mesg').value);
    localStorage.setItem('box', $('#box').is(':checked'));
});

function setLocation(curLoc) {
    history.pushState(null, null, curLoc);
    return;
}

function done() {
    document.getElementById('nn').value = "";
    document.getElementById('mail').value = "";
    document.getElementById('mesg').value = "";
    $('#box').prop('checked', false);
    localStorage.clear();
};

$(document).ready(function () {
    $('#newform').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: 'https://api.slapform.com/cherepanova_katya@inbox.ru',
            dataType: "json",
            method: 'POST',
            data: {
                name: $('#nn').val(),
                email: $('#mail').val(),
                message: document.getElementById('mesg').value,
                check: $('#box').is(':checked'),
                slap_captcha: false
            },
            success: function (response) {
                console.log('Got data: ', response);
                if (response.meta.status == 'success') {
                    $('.modal').modal('hide');
                    alert("Спасибо что ответили!)");
                    done();
                } else if (response.meta.status == 'fail') {
                    $('.modal').modal('hide');
                    alert("ERROR");
                    console.log('Submission failed with these errors: ', response.meta.errors);
                }
            }
        });
    });
});